package hr.ferit.bruno.example_106;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Zoric on 3.10.2017..
 */

public class DemonView extends View  {

    float mX, mY;
    int mWidth, mHeight;
    Bitmap mDemonBitmap;

    public DemonView(Context context) {
        this(context, null, 0);
    }

    public DemonView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DemonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initialize();
    }

    private void initialize() {
        this.mDemonBitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher_round);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.mWidth = MeasureSpec.getSize(widthMeasureSpec);
        this.mHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(this.mWidth,this.mHeight);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.mX = w / 2.0F;
        this.mY = h / 2.0F;
        this.mWidth = w;
        this.mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        float left = this.mX - this.mDemonBitmap.getWidth()/2;
        float top = this.mY - this.mDemonBitmap.getHeight() / 2;
        canvas.drawBitmap(this.mDemonBitmap,left,top,null);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mX = event.getX();
        this.mY = event.getY();
        invalidate();
        return true;
    }
}
